<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePostRequest;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PostController extends Controller
{
    protected $post;

    /**
     * @param $post
     */
    public function __construct(Post $post)
    {
        $this->post = $post;
    }


    public function show(int $id)
    {
        $post = $this->post->findOrFail($id);
        return view('posts.show', compact('post'));
    }

    public function store(CreatePostRequest $request)
    {
        $this->post->create($request->all());
        return redirect()->route('posts.index');
    }
}
