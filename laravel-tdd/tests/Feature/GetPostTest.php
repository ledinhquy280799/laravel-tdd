<?php

namespace Tests\Feature;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetPostTest extends TestCase
{
    /** @test */
    public function user_can_get_post_if_user_login_and_post_exist()
    {
        $this->actingAs(User::factory()->create());
        $post = Post::factory()->create();
        $response = $this->get($this->getGetPostRoute($post->id));
        $response->assertViewIs('posts.show');
        $response->assertStatus(Response::HTTP_OK);
        $response->assertSee($post->title);
    }

    /** @test */
    public function user_can_not_get_post_if_user_login_and_post_not_exist()
    {
        $this->actingAs(User::factory()->create());
        $postId = -1;
        $response = $this->get($this->getGetPostRoute($postId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function user_can_not_get_post_if_user_not_login_and_post_exist()
    {
        $post = Post::factory()->create();
        $response = $this->get($this->getGetPostRoute($post->id));
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }

    public function getGetPostRoute(int $id)
    {
        return route('posts.show', $id);
    }
}
