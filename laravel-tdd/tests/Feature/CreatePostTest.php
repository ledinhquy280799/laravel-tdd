<?php

namespace Tests\Feature;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreatePostTest extends TestCase
{
    /** @test */
    public function user_can_create_post_if_user_login_and_data_valid()
    {
        $this->withoutExceptionHandling();
        $this->actingAs(User::factory()->create());
        $countNumberPostBeforeCreate = Post::count();
        $dataCreate = [
            'title' => $this->faker->name,
            'content' => $this->faker->text
        ];
        $response = $this->post($this->getCreatePostRoute(), $dataCreate);
        $this->assertDatabaseHas('posts', [
            'title' => $dataCreate['title'],
            'content' => $dataCreate['content']
        ]);
        $countNumberPostAfterCreate = Post::count();
        $this->assertEquals($countNumberPostBeforeCreate + 1, $countNumberPostAfterCreate);
        $response->assertRedirect(route('posts.index'));
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function user_can_not_create_post_if_user_login_and_data_invalid()
    {
        $this->actingAs(User::factory()->create());
        $dataCreate = [
            'title' => '',
            'content' => $this->faker->text
        ];
        $response = $this->post($this->getCreatePostRoute(), $dataCreate);
        $response->assertSessionHasErrors(['title']);
    }

    /** @test */
    public function user_can_not_create_post_if_user_not_login_and_data_valid()
    {
        $dataCreate = [
            'title' => $this->faker->name,
            'content' => $this->faker->text
        ];
        $response = $this->post($this->getCreatePostRoute(), $dataCreate);
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }

    public function getCreatePostRoute()
    {
        return route('posts.store');
    }
}
